#pragma once

#include "settings.hpp"
#include "matrix.hpp"

#define NORM_CAUSE false
#define NUMBER_CAUSE true


typedef function< vector<double> (vector<double>& A)> F_PTR;


class Newton_solver {
 private:
    map<string, double> settings;
 public:
    Matrix<double> partial_derivative(F_PTR f, vector<double>& pA, vector<double>& nA, int y_num);
    Matrix<double> create_Jacobi_matrix(F_PTR f, vector<double>& pA, vector<double>& nA);
    Matrix<double> create_residual_vector(F_PTR f, vector<double>& nA);
    vector<double> solve(F_PTR f, vector<double>& pA, vector<double>& A, bool* stop_cause);
    void set_settings(map<string, double> _settings);
};