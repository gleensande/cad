#pragma once
#include <SFML/Graphics.hpp>
#include <map>
#include <vector>
#include <functional>
#include <any>
#include <iostream>
#include <string>
#include <fstream>
#include <math.h>
#include <sstream>
#include <iomanip> 

using std::map;
using std::vector;
using std::pair;
using std::function;
using std::bind;
using std::any;
using std::make_any;
using std::any_cast;
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::stoi;
using std::setw;

#define PI 3.14159265

// color palette
#define BLACK                   sf::Color::Black
#define WHITE                   sf::Color::White
#define DARK_GRAY_GREEN         sf::Color(35, 52, 56 )
#define LIGHT_LIGHT_YELLOW      sf::Color(255, 255, 224)
#define LIGHT_YELLOW            sf::Color(252,224,162)
#define PINK                    sf::Color(217, 89, 200)
#define LIGHT_GRAY              sf::Color(0,0,0,100)

// for ui and workspace
#define BUTTON_COUNT            10
#define PATH_BUTTONS_SETTINGS   "media/buttons.csv"

#define COORD_TEXT_STYLE        sf::Text::Bold
#define SIZE_TEXT_COORD         24
#define THICKNESS_OUTLINE       2.f
#define THICKNESS_OUTLINE_ER    3.f
#define WIDTH_LINE_GRID         1.f
#define STEP_GRID               WIDTH_LINE_GRID*50

#define COLOR_WINDOW            DARK_GRAY_GREEN
#define COLOR_WORKSPACE         LIGHT_LIGHT_YELLOW
#define COLOR_WORKSPACE_OUTLINE PINK
#define COLOR_COORD_TEXT        BLACK
#define COLOR_GRID              LIGHT_GRAY
#define COLOR_POINT             PINK
#define COLOR_LINE              BLACK

// for input 
#define COLOR_INPUT             LIGHT_YELLOW
#define COLOR_INPUT_TEXT        BLACK
#define COLOR_INPUT_OUTLINE     PINK
#define COLOR_INPUT_OUTLINE_ER  sf::Color(250,0,0,100)
#define SIZE_DESCRIPTION_TEXT   24
#define INPUT_SPACE             10
#define ERROR_SPACE             20
#define COLOR_INPUT_USER        LIGHT_LIGHT_YELLOW
#define MAX_INPUT_SIZE          20

// for button 
#define SIZE_TEXT_HINT          14
#define COLOR_TEXT_HINT         WHITE
#define STYLE_TEXT_HINT         sf::Text::Bold
#define PATH_TO_BASE_ICONS      "media/icons/"

// for geometry
#define G_STATE_NONE                0
#define G_STATE_POINT_CREATION      1
#define G_STATE_LINE_CREATION       2
#define G_STATE_P_TO_P_CREATION     3
#define G_STATE_POINTS_DISTANCE     4
#define G_STATE_PARALLEL_CREATION   5
#define G_STATE_PERPEND_CREATION    6
#define G_STATE_ANGLE_SETTING       7
#define G_STATE_GORIZONT_CREATION   8
#define G_STATE_VERTICAL_CREATION   9
#define G_STATE_P_TO_L_CREATION     10

// for system parmeters
#define P_X                         true
#define P_Y                         false

// for newton solver
#define N_MAX_STEPS                 7
#define N_EPS                       1e-6

struct Point_drawable {
 public:
    unsigned int id;
    int x;
    int y;
};

struct Line_drawable {
 public:
    unsigned int id;
    unsigned int p1_id;
    unsigned int p2_id;
};

// common functions
template <typename T>
void vector_print(vector<T> v, string name);