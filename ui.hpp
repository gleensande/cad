#pragma once
#include "settings.hpp"
#include "bus.hpp"
#include "workspace.hpp"
#include "button.hpp"
#include "csv_parser.hpp"
#include "input.hpp"
#include "error.hpp"

class UI {
 private:
    const string buttons_settings_path = PATH_BUTTONS_SETTINGS;

	sf::RenderWindow* window;
    sf::Font font;

    sf::Vector2<int> mouse_position;

    Bus* bus_ptr;
    Workspace ws;
    Button up_menu[BUTTON_COUNT];

    sf:: Image icon;
    Errors errors;
    Input input;

 public:
    void create(sf::RenderWindow* iwindow);
    void draw();
    void create_icon();
    void create_buttons();

    // event handlers
    void handle_events();
    void on_mouse_moved();
    void on_closed();
    void on_resized(sf::Event event);
    void on_click(sf::Event event);
    void on_text_entered(sf::Event event);
    void on_key(sf::Event event);
};
