#pragma once
#include "bus.hpp"
#include "settings.hpp"

class Point {
private:
    int x;
    int y;
    unsigned int id;
    Bus* bus_ptr;
public:
    Point(int _x = 0, int _y = 0, unsigned int _id = 0);
    void change_coordinates(int new_x, int new_y);
    int get_x() const;
    int get_y() const;
    unsigned int get_id() const;
    void add_x(int inc_x);
    void add_y(int inc_y);
};

class Line {
private:
    Point* point1;
    Point* point2;
    unsigned int id;
    Bus* bus_ptr;
public:
    Line(Point* _point1 = NULL, Point* _point2 = NULL, unsigned int _id = 0);
    void change_point1(int x, int y);
    void change_point2(int x, int y);
    int get_x1() ;
    int get_x2() ;
    int get_y1() ;
    int get_y2() ;
    vector<unsigned int> get_points_ids();
    unsigned int get_id() const;
};