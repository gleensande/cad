#include "constraints.hpp"

int Constraint::get_params_num() const {
    return params.size();
}

int Constraint::get_f_size() const {
    return f_size;
}

vector<Parameter*> Constraint::get_params() {
    return params;
}


vector<unsigned int> Constraint::get_param_ids() const {
    vector<unsigned int> result;

    for (int i = 0; i < params.size(); i++) {
        result.push_back(params[i]->param_id);
        cout << "param_id[" << i << "] = " << params[i]->param_id << endl;
    }

    return result;
}

void Point_to_point::create(unsigned int _id, vector<Parameter*> _params) {
    id = _id;
    params = _params;

    cout << "CREATE PARAM IDs" << endl;
    for (int i = 0; i < params.size(); i++) {
        cout << "param_id[" << i << "] = " << params[i]->param_id << endl;
    }

    f_size = 2;
}

vector<double> Point_to_point::f_with_increments(vector<double> _increments) {
    vector<double> f(2);

    f[0] = params[2]->point->get_x() + _increments[2] - params[0]->point->get_x() - _increments[0];
    f[1] = params[3]->point->get_y() + _increments[3] - params[1]->point->get_y() - _increments[1];

    return f;
}

// возвращает вектор производных по заданному параметру
vector<double> Point_to_point::first_derivatives(vector<double> _increment, unsigned int param_id) {    
    vector<double> df;
    int param_num = -1;
    
    for (int i = 0; i < params.size(); i++) {
        if (params[i]->param_id == param_id) {
            param_num = i;
            break;
        }
    }

    // _increment - d_x1 d_y1 d_x2 d_y2
    switch(param_num) {
        case 0: df = {-1, 0}; break;
        case 1: df = {0, -1}; break;
        case 2: df = {1, 0}; break;
        case 3: df = {0, 1}; break;
        case -1: df = {0, 0}; break;
    }

    return df;
}


void Points_distance::create(unsigned int _id, vector<Parameter*> _params) {
    params = _params;
    id = _id;
    f_size = 1;
}

void Points_distance::set_distance(float _distance) {
    distance = _distance;
}

// _increment - d_x1 d_y1 d_x2 d_y2
// params - x1 y1 x2 y2
vector<double> Points_distance::f_with_increments(vector<double> _increment) {
    vector<double> f(1);

    f[0] =  (params[2]->point->get_x() + _increment[2] - params[0]->point->get_x() - _increment[0]) *
            (params[2]->point->get_x() + _increment[2] - params[0]->point->get_x() - _increment[0]) +
            (params[3]->point->get_y() + _increment[3] - params[1]->point->get_y() - _increment[1]) *
            (params[3]->point->get_y() + _increment[3] - params[1]->point->get_y() - _increment[1]) -
            distance * distance;

    return f;
}

vector<double> Points_distance::first_derivatives(vector<double> _increment, unsigned int param_id) {
    vector<double> df;
    int param_num = -1;
    
    for (int i = 0; i < params.size(); i++) {
        if (params[i]->param_id == param_id) {
            param_num = i;
            break;
        }
    }

    // _increment - d_x1 d_y1 d_x2 d_y2
    double  x1 = params[0]->point->get_x(),
            y1 = params[1]->point->get_y(),
            x2 = params[2]->point->get_x(),
            y2 = params[3]->point->get_y(),
            dx1 = _increment[0],
            dy1 = _increment[1],
            dx2 = _increment[2],
            dy2 = _increment[3];


    switch(param_num) {
        case 0: df = {2 * x1 - 2 * x2 + 2 * dx1 - 2 * dx2}; break;
        case 1: df = {2 * y1 - 2 * y2 + 2 * dy1 - 2 * dy2}; break;
        case 2: df = {-2 * x1 + 2 * x2 - 2 * dx1 + 2 * dx2}; break;
        case 3: df = {-2 * y1 + 2 * y2 - 2 * dy1 + 2 * dy2}; break;
        case -1: df = {0}; break;
    }

    return df;
}

void Gorizont_line::create(unsigned int _id, vector<Parameter*> _params){
    params = _params;
    f_size = 1;
    id = _id;
}

// params - y1 y2
vector<double> Gorizont_line::f_with_increments(vector<double> _increment) {
    vector<double> f(1);

    f[0] =  (params[1]->point->get_y() + _increment[1] - params[0]->point->get_y() - _increment[0]);

    return f;
}

vector<double> Gorizont_line::first_derivatives(vector<double> _increment, unsigned int param_id) {
    vector<double> df;
    int param_num = -1;
    
    for (int i = 0; i < params.size(); i++) {
        if (params[i]->param_id == param_id) {
            param_num = i;
            break;
        }
    }

    switch(param_num) {
        case 0: df = {-1}; break;
        case 1: df = {1}; break;
        case -1: df = {0}; break;
    }

    return df;
}



void Vertical_line::create(unsigned int _id, vector<Parameter*> _params) {
    params = _params;
    f_size = 1;
    id = _id;
}

vector<double> Vertical_line::f_with_increments(vector<double> _increment) {
    vector<double> f(1);

    f[0] =  (params[1]->point->get_x() + _increment[1] - params[0]->point->get_x() - _increment[0]);

    return f;
}

vector<double> Vertical_line::first_derivatives(vector<double> _increment, unsigned int param_id) {
    vector<double> df;
    int param_num = -1;
    
    for (int i = 0; i < params.size(); i++) {
        if (params[i]->param_id == param_id) {
            param_num = i;
            break;
        }
    }

    switch(param_num) {
        case 0: df = {-1}; break;
        case 1: df = {1}; break;
        case -1: df = {0}; break;
    }

    return df;
}



/*
void Point_to_line::create(Point* _point1, Line* _line1, unsigned int _id){
    point1 =_point1;
    line1 = _line1;
    id = _id;
}
bool Point_to_line::use(){

}

void Set_angle::create(Line* _line1, Line* _line2, float _angle ,unsigned int _id){
    line1 = _line1;
    line2 = _line2;
    angle = _angle;
    id = _id;
}

void Set_angle::use(){
    int bx =0;
    int by =0;
    int ax =0;
    int ay =0;
    float length_line2 =0;
    float length_line1 =0;
    int x0 =0;
    int y0 =0;

    x0 = line1->get_x1();
    y0 = line1->get_y1();
    cout << "x0 y0 "<< x0 <<" " << y0 << endl;
//a - вектор line1 ; b - вектор line2
    bx = line2->get_x2() - line2->get_x1();
    by = line2->get_y2() - line2->get_y1();
    length_line2= sqrt(bx*bx + by*by);

    cout<<"length_line2 " <<length_line2 << " bx "<< bx << " by "<< by << endl;
    
   
    ax = line1->get_x2() - line1->get_x1();
    ay = line1->get_y2() - line1->get_y1();
    length_line1 = sqrt(ax*ax + ay*ay);

    cout<<"length_line1 " << length_line1 <<" ax "<< ax << " ay "<< ay << endl;
    

//направляющий вектор
    float new_bx = 0.0;
    float new_bxx = 0.0;
    float new_by = 0.0;
    float new_byy = 0.0;
    float length_new_b = 0.0;

    new_by = 1.0;
    new_bx = (length_line2*length_line1*cos(angle*PI / 180) - ay*new_by)/ax;
    cout<<"new bx " << new_bx << endl;
    length_new_b = sqrt(new_bx*new_bx + new_by*new_by);

    cout<<"length_new_b " << length_new_b << endl;
//Единичный вектор * длину исходного вектора = new вектор
    new_byy = new_by/length_new_b*length_line2;
    new_bxx = new_bx/length_new_b*length_line2;
cout<<"new bx pro " <<new_bxx << endl;
cout<<"new by pro " <<new_byy << endl;
//новая координата
    int new_coord_bx =0;
    int new_coord_by =0;
    new_coord_bx = (int) new_bx + line1->get_x1();
    new_coord_by = (int) new_by + line1->get_y1();

    cout<<"new_coord_bx  " << new_coord_bx  << endl;
    cout<<"new_coord_by  " << new_coord_by  << endl;

    line2->change_point1(line1->get_x1(), line1->get_y1());
    line2->change_point2(new_coord_bx,new_coord_by);
    cout<<"coord line 1  " << line1->get_x1()<<"  "<<line1->get_y1() << endl;


}



void Parallel_lines:: create(Line* _line1, Line* _line2, unsigned int _id){
    line1 = _line1;
    line2 = _line2;
    id= _id;
}
bool Parallel_lines::use(){

}

void Perpend_lines:: create(Line* _line1, Line* _line2, unsigned int _id){
    line1 = _line1;
    line2 = _line2;
    id= _id;
}
bool Perpend_lines::use(){

}*/