#include "app.hpp"

int main() {
    App app;
    app.run();
}

/*#include "newton_solver.hpp"
#include "settings.hpp"

vector<double> f(vector<double>& A) {
    vector<double> result(2);
    
    double x = A[0], y = A[1];

    result[0] = 7 * x * y + 2 * x * x - 4 * y * y;
    result[1] = x * x - 5 * x * y + y + 11;

    return result;  
}

int main() {
    map<string, double> settings = {
        {"eps", 10e-6},
        {"max_steps", 40},        
    };
    Newton_solver solver(settings);

    vector<double> first_approx = {1, 2};

    bool stop_cause;
    vector<double> result = solver.solve(f, first_approx, first_approx, &stop_cause);
    
    cout << "Result:" << endl;
    for (auto r : result) {
        cout << r << endl;
    }
}*/
