#include "bus.hpp"

Bus* Bus::instance_ptr = 0;
int Bus::messages_count = 0;

void Bus::handle_messages() {
    // if (messages.size() != 0) {
    //     cout << "-------------" << endl;
    //     cout << "Handle messages: " << endl;
    // }
    for (int i = 0; i < messages.size(); i++) {
        Message message = messages[i];
        if(subscribes.find(message.name) != subscribes.end() ) {
            for (int i = subscribes[message.name].size() - 1; i >= 0; i--) {
                // cout << "Call subcriber of: " << message.name << endl;
                (subscribes[message.name][i])(message.info);
            }
        } else {
            cout << "-------------" << endl;
            cout << "Can't find subscribers for message: " << message.name << endl;
        }
        messages_count--;
    }
    messages.clear();
    
}

void Bus::subscribe(string message_name, function<void (any info)> handler) {
    subscribes[message_name].push_back(handler);
}

void Bus::send_message(Message message) {
    // cout << "-------------" << endl;
    // cout << "Bus get new message: " << message.name << endl;
    messages.push_back(message);
    messages_count++;
}

void Bus::print_subscribes() const {
    cout << "-------------" << endl;
    cout << "Current subscribes:" << endl;
    for (auto s : subscribes) {
        cout << s.first << " : some function" << endl;
    }
} 

void Bus::print_messages() const{
    cout << "-------------" << endl;
    cout << "Current messages:" << endl;
    for (Message m : messages) {
        cout << m.name << endl;
    }
}