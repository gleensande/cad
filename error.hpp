#pragma once
#include "settings.hpp"
#include "bus.hpp"
#include "button.hpp"

class Errors
{
private:
    Bus* bus_ptr;
    sf::Font* font;
    sf::RenderWindow* window;
   
    string description_error;
    sf::Text description_error_text;
    
    sf::Vector2<float> size;
    sf::Vector2<float> position;
    
    sf::RectangleShape main_rect;
    Button close_btn;

    bool display;
 public:
    void create(sf::RenderWindow* iwindow, sf::Font* ifont);
    void draw();
    void set_description(string idescription_error);
    void switch_display();
    void on_resize();
    void on_close_error(any info);
    void on_error_sms(any info);
    
};

