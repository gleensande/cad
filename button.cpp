#include "button.hpp"

void Button::create(vector<string> iinfo, sf::Vector2<float> iposition, sf::RenderWindow* iwindow, sf::Vector2<float> isize, sf::Font* ifont) {
	window = iwindow;
	name = iinfo[0];
	message = iinfo[1];
	path_to_texture = icons_base_path + iinfo[2];
	description = iinfo[3];
	set_position(iposition);

	if (!texture.loadFromFile(path_to_texture))
		cout << "Can't find the image" << endl;

	sprite.setOrigin(0, 0);
	sprite.setTexture(texture);

	set_size(isize);
	

	font = ifont;
	hint.setFont(*font);
    hint.setCharacterSize(SIZE_TEXT_HINT);
    hint.setFillColor(COLOR_TEXT_HINT);
    hint.setStyle(STYLE_TEXT_HINT);
	hint.setString(description);
	display_hint = false;

	// Подписка на событие
	bus_ptr = Bus::get_instance();
	bus_ptr->subscribe("click", bind(&Button::on_click, this, std::placeholders::_1));
}

void Button::draw() {
	window->draw(sprite);
	if (display_hint) {
		window->draw(hint);
	}
}

bool Button::mouse_in_rect(sf::Vector2<float> coordinates) {
    if ((position.x <= coordinates.x) && ((position.x + size.x) >= coordinates.x)
	 && (position.y <= coordinates.y) && ((position.y + size.y) >= coordinates.y)) {
		 return true;
	}

	return false;
}

void Button::on_click(any info) {
	sf::Vector2<float> click_coords = any_cast<sf::Vector2<float> >(info);
	if (mouse_in_rect(click_coords)) {
		Message msg = {message, ""}; 
		bus_ptr->send_message(msg);
	}
}

void Button::on_mouse_moved() {
	sf::Vector2<int> cc_int = sf::Mouse::getPosition(*window);
	sf::Vector2<float> curent_coords = {(float)cc_int.x, (float)cc_int.y};
	display_hint = mouse_in_rect(curent_coords);
}

void Button::set_size(sf::Vector2<float> isize) {
	size = isize;
	icon_size = {sprite.getLocalBounds().width, sprite.getLocalBounds().height};
	sf::Vector2<float> scale = {size.x / icon_size.x , size.y / icon_size.y};
	sprite.setScale(scale);
	hint.setPosition({position.x, position.y + size.y});
}

void Button::set_position(sf::Vector2<float> iposition) {
	position = iposition;
	sprite.setPosition(position);
}
