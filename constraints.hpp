#pragma once
#include "figures.hpp"
#include "parameter.hpp"

class Constraint {
 protected:
    unsigned int id;
    vector<Parameter*> params;
    int f_size;
 public:
    int get_params_num() const;
    int get_f_size() const;
    vector<unsigned int> get_param_ids() const;
    virtual vector<double> f_with_increments(vector<double> _increment) = 0;
    virtual vector<double> first_derivatives(vector<double> _increment, unsigned int param_id) = 0;
    virtual void create(unsigned int _id, vector<Parameter*> _params) = 0;
    vector<Parameter*> get_params();

};

class Point_to_point: public Constraint {
 private:
   // params - x1 y1 x2 y2
 public:
   // _increment - d_x1 d_y1 d_x2 d_y2
   vector<double> f_with_increments(vector<double> _increment);
   vector<double> first_derivatives(vector<double> _increment, unsigned int param_id);
   void create(unsigned int _id, vector<Parameter*> _params);
};

class Points_distance: public Constraint {
 private:
    float distance;
    // params - x1 y1 x2 y2
 public:
    // _increment - d_x1 d_y1 d_x2 d_y2
    vector<double> f_with_increments(vector<double> _increment);
    vector<double> first_derivatives(vector<double> _increment, unsigned int param_id);
    void create(unsigned int _id, vector<Parameter*> _params);
    void set_distance(float _distance);
};

class Gorizont_line: public Constraint {
 private:
  // params - y1 y2
 public:
   void create(unsigned int _id, vector<Parameter*> _params);
   vector<double> f_with_increments(vector<double> _increment);
   vector<double> first_derivatives(vector<double> _increment, unsigned int param_id);
};

class Vertical_line: public Constraint {
 private:
  // params - x1 x2
 public:
  void create(unsigned int _id, vector<Parameter*> _params);
   vector<double> f_with_increments(vector<double> _increment);
   vector<double> first_derivatives(vector<double> _increment, unsigned int param_id);
};


/*class Point_to_line: public Constraint {
 private:
  Point* point1;
  Line* line1;
 public:
  void create(Point* _point1, Line* _line1, unsigned int _id);
  bool use();
};

/*
class Parallel_lines: public Constraint {
 private:
  Line* line1;
  Line* line2;
 public:
  void create(Line* _line1, Line* _line2, unsigned int _id);
  bool use();
};

class Perpend_lines: public Constraint {
 private:
  Line* line1;
  Line* line2;
 public:
  void create(Line* _line1, Line* _line2, unsigned int _id);
  bool use();
};

class Set_angle: public Constraint {
 private:
  Line* line1;
  Line* line2;
  float angle;
 public:
  void create(Line* _line1, Line* _line2, float _angle ,unsigned int _id);
  void use();
};

*/
