#pragma once

#include "settings.hpp"
#include "figures.hpp"

struct Parameter {
 public:
    unsigned int param_id;
    Point* point;
    bool x_or_y;
    
};