#pragma once
#include "settings.hpp"

typedef struct {
 public:
    string name;
    any info;
} Message;


class Bus {
 private:
    static Bus* instance_ptr;
    Bus() {}
    map <string, vector< function<void (any info)> > > subscribes;
    vector<Message> messages;
    static int messages_count;

 public:
    static Bus * get_instance() {
        if (!instance_ptr) {
            instance_ptr = new Bus();
        }
        return instance_ptr;
    }
    void handle_messages();
    void subscribe(string message_name, function<void (any info)> handler);
    void send_message(Message message);
    void print_subscribes() const;
    void print_messages() const;
};  
