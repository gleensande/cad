#pragma once
#include "settings.hpp"
#include "ui.hpp"
#include "geometry.hpp"

class App {
 private:
    sf::RenderWindow window;
    Bus* bus_ptr;
	UI ui;
    Geometry gm;
 public:
    void run();
};
