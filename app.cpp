#include "app.hpp"

void App::run() {
    bus_ptr = Bus::get_instance();
    window.create(sf::VideoMode(1600, 800), "NX2.0 by VNKS");
	ui.create(&window);
	gm.create();
    while (window.isOpen()) {
        ui.handle_events();
        bus_ptr->handle_messages();
        ui.draw();
    }
	
	delete bus_ptr;
}
