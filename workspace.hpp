#pragma once
#include "settings.hpp"
#include "bus.hpp"

class Workspace {
 private:
    const float grid_line_width = WIDTH_LINE_GRID;
    const float grid_step = STEP_GRID;

    map<unsigned int, Point_drawable> points;
    map<unsigned int, sf::Text> point_id_texts;
    map<unsigned int, Line_drawable> lines;
    map<unsigned int, sf::Text> line_id_texts;
    Bus* bus_ptr;

    sf::Text coordinates_text;
    sf::Font *font;
    sf::Vector2<int> mouse_position;

    sf::Vector2<float> size;
    sf::Vector2<float> position;
    sf::RenderWindow *window;
    
    sf::RectangleShape rect;    
    // grid
    int hor_lines_count;
    vector<sf::RectangleShape> hor_lines;
    int vert_lines_count;
    vector<sf::RectangleShape> vert_lines;

 public:
    void create(sf::RenderWindow *iwindow, sf::Font *ifont);
    void prepare_coordinates_text();
    void update_size_and_position();
    void update_coordinates_text();
    void update_grid();
    void draw();
    void update_rect_size_and_pos();
    void update();

    bool mouse_in_rect();

    void add_point(any info);
    void add_line(any info);
    void change_point(any info);

    void draw_geom_figures();
};