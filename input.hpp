#pragma once
#include "settings.hpp"
#include "bus.hpp"
#include "button.hpp"

class Input {
 private:
	Bus* bus_ptr;
   sf::Font* font;
   sf::RenderWindow* window;

   sf::Vector2<float> size;
   sf::Vector2<float> position;

   sf::RectangleShape main_rect;
   sf::RectangleShape user_rect;

   string description;
   sf::Text description_text;

   string user_string;
   sf::Text user_text;

   bool display;
 public:
    void create(sf::RenderWindow* iwindow, sf::Font* ifont);
    void draw();
    void set_description(string idescription);
    void switch_display();
    void on_resize();
    void on_text_entered(char c);
    void on_enter();
    void on_start_input(any info);
};