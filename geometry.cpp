#include "geometry.hpp"

void Geometry::create() {
    object_id = 0;
    param_id = 0;
    wait_for_input = false;
    state = G_STATE_NONE;

    bus_ptr = Bus::get_instance();
    bus_ptr->subscribe("add_linesegment_pressed",       bind(&Geometry::on_add_linesegment_pressed,     this, std::placeholders::_1));
    bus_ptr->subscribe("add_point_pressed",             bind(&Geometry::on_add_point_pressed,           this, std::placeholders::_1));

    bus_ptr->subscribe("add_pointtopoint_pressed",      bind(&Geometry::on_add_pointtopoint_pressed,    this, std::placeholders::_1));
    bus_ptr->subscribe("add_distancelimit_pressed",     bind(&Geometry::on_add_distancelimit_pressed,   this, std::placeholders::_1));
    bus_ptr->subscribe("add_parallellimit_pressed",     bind(&Geometry::on_add_parallellimit_pressed,   this, std::placeholders::_1));
    bus_ptr->subscribe("add_perpendllimit_pressed",     bind(&Geometry::on_add_perpendlimit_pressed,    this, std::placeholders::_1));
    bus_ptr->subscribe("add_anglelimit_pressed",        bind(&Geometry::on_add_anglelimit_pressed,      this, std::placeholders::_1));
    bus_ptr->subscribe("add_gorizontlimit_pressed",     bind(&Geometry::on_add_gorizontlimit_pressed,   this, std::placeholders::_1));
    bus_ptr->subscribe("add_verticallimit_pressed",     bind(&Geometry::on_add_verticallimit_pressed,   this, std::placeholders::_1));
    bus_ptr->subscribe("add_pointtolinelimit_pressed",  bind(&Geometry::on_add_pointtolinelimit_pressed,this, std::placeholders::_1));

    bus_ptr->subscribe("input_finished", bind(&Geometry::on_input_finished, this, std::placeholders::_1));

    map<string, double> settings = {
        {"eps", 10e-6},
        {"max_steps", 40},        
    };
    NS.set_settings(settings);
}

void Geometry::on_add_linesegment_pressed(any info) {
    string description = "Enter 4 coordinates (space between) - x1, y1, x2, y2:";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;
    state = G_STATE_LINE_CREATION;
}

void Geometry::create_line(int x1, int y1, int x2, int y2) {
    // создание объекта геометрии
    int p1_id = create_point(x1, y1);
    int p2_id = create_point(x2, y2);

    lines.insert_or_assign(object_id, Line(&(points[p1_id]), &(points[p2_id]), object_id));

    object_id++;
}

// возвращает id созданного элемента в словаре Points
int Geometry::create_point(int x, int y) {
    cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
    cout << "before create point" << endl;
    print_constraints();
    points.insert_or_assign(object_id, Point(x, y, object_id));

    // создание нужных параметров системы
    Parameter* new_param = new Parameter;
    *new_param = {param_id, &(points[object_id]), P_X};
    params.push_back(new_param);
    param_id++;

    new_param = new Parameter;
    *new_param = {param_id, &(points[object_id]), P_Y};
    params.push_back(new_param);
    param_id++;

    print_params();

    object_id++;
   
    cout << "after create point" << endl;
    print_constraints();
    cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;

    return object_id - 1;
}

void Geometry::print_params() {
    cout << "SYSTEM PARAMS = " << endl;
    for (int i = 0; i < params.size(); i++) {
        cout << "param_id=" << params[i]->param_id << ", ";
        cout << "point_id=" << params[i]->point->get_id() << ", ";
        cout << "x_or_y=" << (params[i]->x_or_y ? "X" : "Y") << endl;
    }


}

void Geometry::on_add_point_pressed(any info) {
    string description = "Enter 2 coordinates (space between):";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;
    state = G_STATE_POINT_CREATION;
}

//начало ограничений
void Geometry::on_add_pointtopoint_pressed(any info) {
    string description = "Enter 2 points id (space between):";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;
    state = G_STATE_P_TO_P_CREATION;
}

vector<Parameter*> Geometry::get_params_by_point_id(unsigned int _id) {
    vector<Parameter*> point_params;
    for (int i = 0; i < params.size(); i++) {
        if (_id == params[i]->point->get_id()) {
            point_params.push_back(params[i]);
        }
    }
    if (point_params.size() != 2) {
        cout << "ОШИБКА: параметры точки не найдены" << endl;
        return point_params;
    }

    return point_params;
}

void Geometry::create_point_to_point(unsigned int id_1, unsigned int id_2) {
    cout << "before p-to-p\n";
    print_constraints();
    Point_to_point* ptp_constr = new Point_to_point;
    vector<Parameter*> p1_params = get_params_by_point_id(id_1);
    vector<Parameter*> p2_params = get_params_by_point_id(id_2);
    vector<Parameter*> points_params = {p1_params[0], p1_params[1], p2_params[0], p2_params[1]};

    constraints.push_back(ptp_constr);
    constraints[constraints.size() - 1]->create(object_id, points_params);
    solve_constraints();
    object_id++;
    state = G_STATE_NONE;
    print_constraints();
    cout << "arter p-to-p\n";
}

void Geometry::on_add_distancelimit_pressed(any info) {
    string description = "Enter 2 points id and a distance (space between):";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;
    state = G_STATE_POINTS_DISTANCE;
}

void Geometry::create_points_distance(unsigned int id_1, unsigned int id_2, float distance) {
    Points_distance* pd_constr = new Points_distance;
    vector<Parameter*> p1_params = get_params_by_point_id(id_1);
    vector<Parameter*> p2_params = get_params_by_point_id(id_2);
    vector<Parameter*> points_params = {p1_params[0], p1_params[1], p2_params[0], p2_params[1]};

    pd_constr->create(object_id, points_params);
    pd_constr->set_distance(distance);
    constraints.push_back(pd_constr);
    solve_constraints();
    object_id++;
    state = G_STATE_NONE;
}

void Geometry::on_add_parallellimit_pressed(any info) {
    string description = "Enter 2 lines id (space between):";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;
    state = G_STATE_PARALLEL_CREATION;
}

void Geometry::create_parallel(unsigned int id_1, unsigned int id_2){
    // Parallel_lines pd_constr;
    // pd_constr.create(&(lines[id_1]), &(lines[id_2]), object_id);
    // pd_constr.use();
    // constraints.push_back(pd_constr);
    // object_id++;
}

void Geometry::on_add_perpendlimit_pressed(any info) {
    string description = "Enter 2 lines id (space between):";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;
    state = G_STATE_VERTICAL_CREATION ;
}

void Geometry::create_perpend(unsigned int id_1, unsigned int id_2){
    // Perpend_lines pd_constr;
    // pd_constr.create(&(lines[id_1]), &(lines[id_2]), object_id);
    // pd_constr.use();
    // constraints.push_back(pd_constr);
    // object_id++;
}

void Geometry::on_add_anglelimit_pressed(any info) {
    string description = "Enter 2 lines id and angle (space between):";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;

    state = G_STATE_ANGLE_SETTING;
}

void Geometry::create_anglelimit(unsigned int id_1, unsigned int id_2, float angle) {
    // Set_angle ptp_constr;
    // ptp_constr.create(&(lines[id_1]), &(lines[id_2]), angle, object_id);
    // ptp_constr.use();
    // constraints.push_back(ptp_constr);
    // object_id++;
}

void Geometry::on_add_gorizontlimit_pressed(any info) {
    string description = "Enter 1 line id:";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;
    state = G_STATE_GORIZONT_CREATION;
}

void Geometry::create_gorizont(unsigned int id_1){
    Gorizont_line* vert_constr = new Gorizont_line;
    vector<unsigned int> points_ids = lines[id_1].get_points_ids();
    vector<Parameter*> p1_params = get_params_by_point_id(points_ids[2]);
    vector<Parameter*> p2_params = get_params_by_point_id(points_ids[3]);
    vector<Parameter*> points_params = {p1_params[1], p2_params[1]};

    vert_constr->create(object_id, points_params);
    constraints.push_back(vert_constr);
    solve_constraints();
    object_id++;
    state = G_STATE_NONE;
}

void Geometry::on_add_verticallimit_pressed(any info) {
    string description = "Enter 1 line id:";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;
    state = G_STATE_VERTICAL_CREATION;
}

void Geometry::create_vertical(unsigned int id_1){
    Vertical_line* vert_constr = new Vertical_line;
    vector<unsigned int> points_ids = lines[id_1].get_points_ids();
    vector<Parameter*> p1_params = get_params_by_point_id(points_ids[0]);
    vector<Parameter*> p2_params = get_params_by_point_id(points_ids[1]);
    vector<Parameter*> points_params = {p1_params[0], p2_params[0]};

    vert_constr->create(object_id, points_params);
    constraints.push_back(vert_constr);
    solve_constraints();
    object_id++;
    state = G_STATE_NONE;
}

void Geometry::on_add_pointtolinelimit_pressed(any info) {
    string description = "Enter 1 point id and 1 line id (space between):";
    Message msg = {"start_input", description};
    bus_ptr->send_message(msg);

    wait_for_input = true;
    state = G_STATE_P_TO_L_CREATION;
}

void Geometry::create_point_to_line(unsigned int id_1,  unsigned int id_2){
    // Point_to_line pd_constr;
    // pd_constr.create(&(points[id_1]),&(lines[id_2]), object_id);
    // pd_constr.use();
    // constraints.push_back(pd_constr);
    // object_id++;
}
//конец ограничений

void Geometry::on_input_finished(any info) {
    if (!wait_for_input) {
        return;
    }

    string user_input = any_cast<string>(info);
    vector<int> input_nums;

    if (state == G_STATE_POINT_CREATION) {
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 2) {
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);
            
            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }

        create_point(input_nums[0], input_nums[1]);

    } else if (state == G_STATE_LINE_CREATION) {
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 4) {
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);
            
            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }

        create_line(input_nums[0], input_nums[1], input_nums[2], input_nums[3]);

    } else if (state == G_STATE_POINTS_DISTANCE) {
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 3) {
            
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);

            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }

        create_points_distance(input_nums[0], input_nums[1], input_nums[2]);

    } else if (state == G_STATE_P_TO_P_CREATION) {
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 2) {
            
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);

            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }
        create_point_to_point(input_nums[0], input_nums[1]);

    } else if (state == G_STATE_PARALLEL_CREATION){
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 2) {
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);
            
            
            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }
        create_parallel(input_nums[0], input_nums[1]);

    } else if (state == G_STATE_PERPEND_CREATION){
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 2) {
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);
            
            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }
        create_perpend(input_nums[0], input_nums[1]);

    } else if (state == G_STATE_ANGLE_SETTING){
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 3) {
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);
            
            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }
        create_anglelimit(input_nums[0], input_nums[1],input_nums[2]);
        
    } else if (state == G_STATE_GORIZONT_CREATION){
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 1) {
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);
            
            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }
        create_gorizont(input_nums[0]);
        
    } else if (state == G_STATE_VERTICAL_CREATION){
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 1) {
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);
            
            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }
        create_vertical(input_nums[0]);
        
    } else if (state == G_STATE_P_TO_L_CREATION){
        input_nums = CSV_Parser::parse_num_line(user_input, " ");

        if (input_nums.size() != 2) {
            string description_error = "Error: wrong input nums";
            Message msg = {"error_sms", description_error};
            bus_ptr->send_message(msg);
            
            state = G_STATE_NONE;
            wait_for_input = false;

            return;
        }
        create_point_to_line(input_nums[0],input_nums[1]);
        
    } 

    state = G_STATE_NONE;
    wait_for_input = false;
}


// ------------------- решение системы для ограничений ------------------- //

void Geometry::solve_constraints() {
    cout << "-------------" << endl;
    cout << "Solving:" << endl;
    print_constraints();
    
    // очищаем в полях класса данные о прошлом решении
    Link_param_X = {};
    Link_constr_param = {};

    // вычисляем различные размеры-константы для системы
    n = 0;          // суммарное количество неизвестных параметров d_p
    m = 0;          // суммарная размерность всех уравнений ограничений = количество коэффициентов Лагранжа lambda

    for (int i = 0; i < constraints.size(); i++) {
        m += constraints[i]->get_f_size();
    }

    cout << "m = " << m << endl;

    // линковка 1 - показываем, приращения каких параметров в каких местах вектора X находятся
    // последние m элементов вектора X - лямбды из метода Лагранжа
    vector<unsigned int> param_ids;
    for (int i = 0; i < constraints.size(); i++) {
        param_ids = constraints[i]->get_param_ids();
        
        for (int j = 0; j < param_ids.size(); j++) {
            if (find(Link_param_X.begin(), Link_param_X.end(), param_ids[j]) == Link_param_X.end()) {
                Link_param_X.push_back(param_ids[j]);
                n++;
            }
        }
    }
    if (Link_param_X.size() != n) {
        cout << "ОШИБКА: Link_param_X.size() != n" << endl;
        return;
    }
    cout << "n = " << n << endl;
    cout << "-------------" << endl;
    cout << "Link params & X:" << endl;
    for (int i = 0; i < Link_param_X.size(); i++) {
        cout << Link_param_X[i] << endl;
    }

    // линковка 2 - показываем соответсвие ограничение -:- номера используемых параметров,
    Link_constr_param.resize(constraints.size());
    for (int i = 0; i < constraints.size(); i++) {
        Link_constr_param[i] = constraints[i]->get_param_ids();
    }
    cout << "-------------" << endl;
    cout << "Link constraints & X:" << endl;
    for (int i = 0; i < Link_constr_param.size(); i++) {
        cout << "C_num = " << i << ", params = ";
        for (int j = 0; j < Link_constr_param[i].size(); j++) {
            cout <<  Link_constr_param[i][j] << ", ";
        }
        cout << endl;
    }

    // ПРОВЕРКА F
    vector<double> X_check(n + m, 0);
    check_F(X_check);
    
    // решение системы
    vector<double> X(n + m, 0);    // cоздаем вектор неизвестных, размером n + m
    bool stop_cause;
    vector<double> new_X = NS.solve(bind(&Geometry::F, this, std::placeholders::_1), X, X, &stop_cause);
    vector<double> m1 = {-1};
    if (stop_cause == NUMBER_CAUSE) {
        constraints.pop_back();

        string description_error = "Newton's method doesn't convergence";
        Message msg = {"error_sms", description_error};
        bus_ptr->send_message(msg);
        
        return;
    }
    if (new_X == m1) {
        constraints.pop_back();

        string description_error = "System can't be solved";
        Message msg = {"error_sms", description_error};
        bus_ptr->send_message(msg);
        
        return;
    }

    vector_print(new_X, "new_X");

    // применение полученного результата
    for (int i = 0; i < Link_param_X.size(); i++) {
        cout << "new_X[i] = " << new_X[i] << endl;
        cout << "params[Link_param_X[i]]= " << params[Link_param_X[i]]->param_id << endl;
        if (params[Link_param_X[i]]->x_or_y == P_X) {
            params[Link_param_X[i]]->point->add_x(new_X[i]);
        } else {
            params[Link_param_X[i]]->point->add_y(new_X[i]);
        }
    }

}

vector<int> Geometry::find_x_indices_by_param_ids(vector<unsigned int>& param_ids) const {
    vector<int> x_indices;

    for (int k = 0; k < param_ids.size(); k++) {
        for (int i = 0; i < Link_param_X.size(); i++) {
            if (Link_param_X[i] == param_ids[k]) {
                x_indices.push_back(i);
            }
        }
    }
    
    return x_indices;
}

vector<double> Geometry::F(vector<double>& X) {
    vector<double> result;

    vector< vector<double> > all_constraint_increments;

    // верхняя часть системы - набор функций f для ограничений
    for (int i = 0; i < constraints.size(); i++) {
        vector<double> one_constraint_f;
        vector<double> one_constraint_increments;
        vector<int> one_constraint_x_indices;
        vector<unsigned int> one_constraint_param_ids;

        // получаем индексы параметров в векторе X для данного ограничения
        one_constraint_x_indices = find_x_indices_by_param_ids(Link_constr_param[i]);
        // cout << "f(" << i << ")" << endl;
        // vector_print(one_constraint_x_indices, "one_constraint_x_indices");

        // из вектора X получаем приращения параметров
        for (int j = 0; j < one_constraint_x_indices.size(); j++) {
            one_constraint_increments.push_back(X[one_constraint_x_indices[j]]);
        }
        all_constraint_increments.push_back(one_constraint_increments);
        // vector_print(one_constraint_increments, "one_constraint_increments");        

        // получаем значение вектор-функции f для одного ограничения
        one_constraint_f = constraints[i]->f_with_increments(one_constraint_increments);
        // vector_print(one_constraint_f, "one_constraint_f");        

        // поочередно помещаем значения f в результат
        for (int j = 0; j < one_constraint_f.size(); j++) {
            result.push_back(one_constraint_f[j]);
        }
    }

    // нижняя часть - дополнение системы по методу Лагранжа
    // всего n уравнений, неизвестные lambda_i в векторе X начинаются c элемента n
    vector<double> one_df_by_p;
    vector<double> L(n);
    // цикл по номеру параметра, сколько параметров, столько и элементов L
    for (int k = 0; k < n; k++) {
        // cout << "L = X[" << k << "]";
        L[k] = X[k];                // приращение соотв. параметра
       
        // цикл по количеству ограничений (внешняя сумма в одном элементе L)
        int l_num = 0;
        for (int i = 0; i < constraints.size(); i++) {
            one_df_by_p = constraints[i]->first_derivatives(all_constraint_increments[i], Link_param_X[k]);
            
            for (int j = 0; j < one_df_by_p.size(); j++) {
                // cout << " + X[" << n + l_num << "] * constr=" << i << ", ind_in_constr=" << j << ", param num = " << Link_param_X[k] << endl;
                L[k] += X[n + l_num] * one_df_by_p[j];
                l_num++;
            }
        }
        // cout << endl;
    }
    // vector_print(L, "L");

    for (int i = 0; i < L.size(); i++) {
        result.push_back(L[i]);
    }

    // vector_print(result, "F");
    return result;
}

vector<double> Geometry::check_F(vector<double>& X) {
    vector<double> result;

    vector< vector<double> > all_constraint_increments;

    // верхняя часть системы - набор функций f для ограничений
    for (int i = 0; i < constraints.size(); i++) {
        vector<double> one_constraint_f;
        vector<double> one_constraint_increments;
        vector<int> one_constraint_x_indices;
        vector<unsigned int> one_constraint_param_ids;

        // получаем индексы параметров в векторе X для данного ограничения
        one_constraint_x_indices = find_x_indices_by_param_ids(Link_constr_param[i]);
        cout << "f(" << i << ")" << endl;
        vector_print(one_constraint_x_indices, "one_constraint_x_indices");

        // из вектора X получаем приращения параметров
        for (int j = 0; j < one_constraint_x_indices.size(); j++) {
            one_constraint_increments.push_back(X[one_constraint_x_indices[j]]);
        }
        all_constraint_increments.push_back(one_constraint_increments);
        vector_print(one_constraint_increments, "one_constraint_increments");        

        // получаем значение вектор-функции f для одного ограничения
        one_constraint_f = constraints[i]->f_with_increments(one_constraint_increments);
        vector_print(one_constraint_f, "one_constraint_f");        

        // поочередно помещаем значения f в результат
        for (int j = 0; j < one_constraint_f.size(); j++) {
            result.push_back(one_constraint_f[j]);
        }
    }

    // нижняя часть - дополнение системы по методу Лагранжа
    // всего n уравнений, неизвестные lambda_i в векторе X начинаются c элемента n
    vector<double> one_df_by_p;
    vector<double> L(n);
    // цикл по номеру параметра, сколько параметров, столько и элементов L
    for (int k = 0; k < n; k++) {
        cout << "L = X[" << k << "]";
        L[k] = X[k];                // приращение соотв. параметра
       
        // цикл по количеству ограничений (внешняя сумма в одном элементе L)
        int l_num = 0;
        for (int i = 0; i < constraints.size(); i++) {
            one_df_by_p = constraints[i]->first_derivatives(all_constraint_increments[i], Link_param_X[k]);
            
            for (int j = 0; j < one_df_by_p.size(); j++) {
                cout << " + X[" << n + l_num << "] * constr=" << i << ", ind_in_constr=" << j << ", param num = " << Link_param_X[k] << endl;
                L[k] += X[n + l_num] * one_df_by_p[j];
                l_num++;
            }
        }
        cout << endl;
    }
    vector_print(L, "L");

    for (int i = 0; i < L.size(); i++) {
        result.push_back(L[i]);
    }

    vector_print(result, "F");
    return result;
}

Geometry::~Geometry() {
    for (int i = 0; i < constraints.size(); i++) {
        delete constraints[i];
    }
}

void Geometry::print_constraints() {
    vector<unsigned int> ids;
    vector<Parameter*> parameters;
    for (int i = 0; i < constraints.size(); i++) {
        ids = constraints[i]->get_param_ids();
        vector_print(ids, "constr_params_ids");
        parameters = constraints[i]->get_params();
        cout << "constr_params: (" << i  << ")" << endl;;
        for (int j = 0; j < parameters.size(); j++) {
            cout << parameters[j]->param_id << " " << parameters[j]->point->get_id() << endl;
        }
    }
}