#pragma once
#include "settings.hpp"
#include "bus.hpp"

class Button {
 private:
	Bus* bus_ptr;
    const string icons_base_path = PATH_TO_BASE_ICONS;
	sf::Texture texture;
	sf::Sprite sprite;
	sf::Vector2<float> position;
	sf::Vector2<float> icon_size;
	sf::Vector2<float> size;
	sf::RenderWindow* window;

	sf::Font* font;
	sf::Text hint;
	bool display_hint;

	string message;
	string path_to_texture;
	string description;
 public:
 	string name;
	void create(vector<string> info, sf::Vector2<float> iposition, sf::RenderWindow* iwindow, sf::Vector2<float> isize, sf::Font* ifont);
	void draw();
	bool mouse_in_rect(sf::Vector2<float> coordinates);
	void on_click(any info);
	void on_mouse_moved();

	void set_size(sf::Vector2<float> isize);
	void set_position(sf::Vector2<float> iposition);
};
