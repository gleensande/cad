#pragma once
#include "settings.hpp"

class CSV_Parser {
 public:
    static vector<string> parse_line(string line, string delimiter = ";");
    static vector<int> parse_num_line(string line, string delimiter = ";");
    static string trim_spaces(string line);
    static map<string, double> parse_kv_file(string filename);  
};
