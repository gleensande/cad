#include "newton_solver.hpp"

vector<double> operator+(vector<double> a, vector<double> b) {
    vector<double> result;
    if (a.size() != b.size()) {
        cout << "ОШИБКА: operator+ - Размеры векторов не совпадают" << endl;
        return result;
    }
    result = a;

    for (int i = 0; i < a.size(); i++) {
        result[i] += b[i];
    }

    return result;
}

// расчет равномерной нормы для словаря
double inf_norm(vector<double>& m) {
    double max_el = -1000000;
    for (int i = 0; i < m.size(); i++) {
        if (m[i] > max_el) {
            max_el = m[i];
        };
    }

    return max_el;
}

Matrix<double> Newton_solver::partial_derivative(F_PTR f, vector<double>& pA, vector<double>& nA, int y_num) {
    vector<double> A_forw = nA;

    double d_l = 0.01;

    int i = 0;
    A_forw[y_num] += d_l;

    Matrix<double> result = (create_residual_vector(f, A_forw) - create_residual_vector(f, nA)) / d_l;

    return result;
}

Matrix<double> Newton_solver::create_Jacobi_matrix(F_PTR f, vector<double>& pA, vector<double>& nA) {
    size_t result_size = pA.size();
    Matrix<double> J(result_size, result_size);

    for (size_t i = 0; i < result_size; i++) {
        J.set_block(partial_derivative(f, pA, nA, i), 0, i);
    }

    return J;
}

Matrix<double> Newton_solver::create_residual_vector(F_PTR f, vector<double>& nA) {
    vector<double> F = f(nA);
    Matrix<double> F_matrix(F);

    return F;
}


vector<double> Newton_solver::solve(F_PTR f, vector<double>& pA, vector<double>& A, bool* stop_cause) {
    double norm = settings["eps"] + 1;
    vector<double> nA = A;
    int n = 0;      // счетчик на случай зацикливания метода

    while (norm > settings["eps"] && n <= settings["max_steps"]) {
        n++;

        Matrix<double> J = create_Jacobi_matrix(f, pA, nA);
        Matrix<double> R = create_residual_vector(f, nA);
        R *= -1;
        J.print_SLAU(&R);
        
        if (J.Gauss(&R) == false) {
            return {-1};
        }
        J.print_SLAU(&R);

        vector<double> R_vec = R.to_vector();
        norm = inf_norm(R_vec);

        nA = nA + R_vec;
    };

    if (norm <= settings["eps"]) {
        *stop_cause = NORM_CAUSE;
    } else {
        *stop_cause = NUMBER_CAUSE;
    }

    return nA;
}

void Newton_solver::set_settings(map<string, double> _settings) {
    settings = _settings;
}
