#include "input.hpp"

void Input::create(sf::RenderWindow* iwindow, sf::Font* ifont) {
    font = ifont;
    window = iwindow;
    
    display = false;

    description_text.setFont(*font);
    description_text.setCharacterSize(SIZE_DESCRIPTION_TEXT);
    description_text.setFillColor(COLOR_INPUT_TEXT);

    user_text.setFont(*font);
    user_text.setCharacterSize(SIZE_DESCRIPTION_TEXT);
    user_text.setFillColor(COLOR_INPUT_TEXT);  

    main_rect.setFillColor(COLOR_INPUT); 
    main_rect.setOutlineColor(COLOR_INPUT_OUTLINE);
    main_rect.setOutlineThickness(THICKNESS_OUTLINE);

    user_rect.setFillColor(COLOR_INPUT_USER); 
    user_rect.setOutlineColor(COLOR_INPUT_OUTLINE);
    user_rect.setOutlineThickness(THICKNESS_OUTLINE);

    bus_ptr = Bus::get_instance();
    bus_ptr->subscribe("start_input", bind(&Input::on_start_input, this, std::placeholders::_1));
}

void Input::draw() {
    if (display) {
        window->draw(main_rect);
        window->draw(description_text);
        window->draw(user_rect);
        window->draw(user_text);
    }
}

void Input::set_description(string idescription) {
    description = idescription;
    description_text.setString(description);

    float height = description_text.getLocalBounds().height;
    float width = description_text.getLocalBounds().width;
    sf::Vector2<float> text_size = {width, height};
    sf::Vector2u window_size = window->getSize();

    size = {text_size.x + 4 * INPUT_SPACE, 2 * text_size.y + 5 * INPUT_SPACE};

    position = {window_size.x / 2 - size.x / 2, window_size.y / 2 - size.y / 2};
    description_text.setPosition({position.x + 2 * INPUT_SPACE, position.y + INPUT_SPACE});
    
    main_rect.setSize(size);
    main_rect.setPosition(position);

    user_rect.setSize({text_size.x + 2 * INPUT_SPACE, text_size.y + 2 * INPUT_SPACE});
    user_rect.setPosition({position.x + INPUT_SPACE, position.y + 2 * INPUT_SPACE + text_size.y});

    user_text.setPosition({position.x + 2 * INPUT_SPACE, position.y + 3 * INPUT_SPACE + text_size.y});

    user_string = "";
    user_text.setString("Print here");
}

void Input::on_resize() {
    set_description(description);
}

void Input::switch_display() {
    display = !display;
}

void Input::on_text_entered(char c) {
    if (display == true) {
        if ((c >= '0' && c <= '9' || c == ' ') && user_string.size() != MAX_INPUT_SIZE) {
            user_string += c;
            user_text.setString(user_string);
        } else if (c == '\b' && user_string.size() != 0) {
            user_string.erase(user_string.length() - 1, 1);
            user_text.setString(user_string);
        }
    }
}

void Input::on_enter() {
    if (display == true) {
        Message msg = {"input_finished", user_string}; 
        bus_ptr->send_message(msg);
        switch_display();
    }
}

void Input::on_start_input(any info) {
    string description = any_cast<string>(info);
    set_description(description);
    switch_display();
}
