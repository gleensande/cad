#include "workspace.hpp"

void Workspace::create(sf::RenderWindow *iwindow, sf::Font *ifont) {
    window = iwindow;
    font = ifont;
    update_size_and_position();

    rect.setFillColor(COLOR_WORKSPACE); 
    rect.setOutlineColor(COLOR_WORKSPACE_OUTLINE); 
    rect.setOutlineThickness(THICKNESS_OUTLINE);
    update_rect_size_and_pos();

    prepare_coordinates_text();
    update_coordinates_text();
    update_grid();

    bus_ptr = Bus::get_instance();
    bus_ptr->subscribe("add_point_to_ws", bind(&Workspace::add_point, this, std::placeholders::_1));
    bus_ptr->subscribe("add_line_to_ws", bind(&Workspace::add_line, this, std::placeholders::_1));
    bus_ptr->subscribe("change_point_in_ws", bind(&Workspace::change_point, this, std::placeholders::_1));
}

void Workspace::update_size_and_position() {
    // весь экран 16*4 х 8*4 = 64 х 32 - чтобы соблюсти ~стандартные пропорции
    // один блок по x - это 1/64 от ширины экрана, получается, аналогично y
    float block_x = 1.f / 64.f * window->getSize().x;
    float block_y = 1.f / 32.f * window->getSize().y;

    size = {62.f * block_x, 24.f * block_y};
    position = {block_x, 7.f * block_y};
}

void Workspace::prepare_coordinates_text() {
    coordinates_text.setFont(*font);
    coordinates_text.setCharacterSize(SIZE_TEXT_COORD);
    coordinates_text.setFillColor(COLOR_COORD_TEXT);
    coordinates_text.setStyle(COORD_TEXT_STYLE);
    coordinates_text.setPosition(position);
}

void Workspace::draw() {
    window->draw(rect);
    window->draw(coordinates_text);
    for (int i = 0; i < hor_lines_count; i++) {
        window->draw(hor_lines[i]);
    }
    for (int i = 0; i < vert_lines_count; i++) {
        window->draw(vert_lines[i]);
    }
    draw_geom_figures();
}

void Workspace::update_coordinates_text() {
    mouse_position = sf::Mouse::getPosition(*window);
    if (mouse_in_rect()) {
        mouse_position.x -= position.x;
        mouse_position.y -= position.y;
        coordinates_text.setString(std::to_string(mouse_position.x) + ";" + std::to_string(mouse_position.y));
    } else {
        coordinates_text.setString("X;Y");
    }
}

void Workspace::update_grid() {
    hor_lines_count = size.y / grid_step + 1;
    hor_lines.resize(hor_lines_count);
    for (int i = 0; i < hor_lines_count; i++) {
        hor_lines[i].setSize(sf::Vector2<float>(size.x, grid_line_width));
        hor_lines[i].setPosition(sf::Vector2<float>(position.x, i * grid_step + position.y));
        hor_lines[i].setFillColor(COLOR_GRID); 
    }

    vert_lines_count = size.x / grid_step + 1;
    vert_lines.resize(vert_lines_count);
    for (int i = 0; i < vert_lines_count; i++) {
        vert_lines[i].setSize(sf::Vector2<float>(grid_line_width, size.y));
        vert_lines[i].setPosition(sf::Vector2<float>(i * grid_step + position.x, position.y));
        vert_lines[i].setFillColor(COLOR_GRID); 
    }
}

void Workspace::update_rect_size_and_pos() {
    rect.setSize(size);
    rect.setPosition(position);
}

void Workspace::update() {
    update_size_and_position();
    coordinates_text.setPosition(position);
    update_rect_size_and_pos();
    update_grid();
}


bool Workspace::mouse_in_rect() {
    if ((position.x <= mouse_position.x) && ((position.x + size.x) >= mouse_position.x)
	 && (position.y <= mouse_position.y) && ((position.y + size.y) >= mouse_position.y)) {
		return true;
	}

    return false;
}

void Workspace::draw_geom_figures() {
    int point_size = 4;
    if (!points.empty()) {
        for (auto p : points) {
            sf::CircleShape point(point_size);
            point.setFillColor(COLOR_POINT);

            point.move(p.second.x + position.x - point_size, p.second.y + position.y - point_size);
            window->draw(point);

            point_id_texts[p.first].setPosition(p.second.x + position.x, p.second.y + position.y);
            window->draw(point_id_texts[p.first]);
        }
    }

    double line_x, line_y;
    if (!lines.empty()) {
        for (int i = 0; i < lines.size(); i++) {
            sf::VertexArray drawable_line(sf::Lines, 4);

            int p1_id = lines[i].p1_id;
            int p2_id = lines[i].p2_id;

            drawable_line[0].position = sf::Vector2f(points[p1_id].x + position.x,
                                                     points[p1_id].y + position.y);
            drawable_line[0].color = COLOR_LINE;
            drawable_line[1].position = sf::Vector2f(points[p2_id].x + position.x,
                                                     points[p2_id].y + position.y);
            drawable_line[1].color = COLOR_LINE;
            window->draw(drawable_line);

            if (points[p1_id].x + position.x > points[p2_id].x + position.x) {
                line_x = points[p1_id].x + position.x + points[p2_id].x + position.x;
            } else {
                line_x = points[p2_id].x + position.x + points[p1_id].x + position.x;
            }
            line_x /= 2;

            if (points[p1_id].y + position.y > points[p2_id].y + position.y) {
                line_y = points[p1_id].y + position.y + points[p2_id].y + position.y;
            } else {
                line_y = points[p2_id].y + position.y + points[p1_id].y + position.y;
            }
            line_y /= 2;

            line_id_texts[lines[i].id].setPosition(line_x, line_y);
            window->draw(line_id_texts[lines[i].id]);
        }
    }
}

void Workspace::add_point(any info) {
    Point_drawable new_point = any_cast< Point_drawable >(info);
    points.insert_or_assign(new_point.id, new_point);


    sf::Text id_text;
    id_text.setString(std::to_string(new_point.id));
    id_text.setFont(*font);
    id_text.setCharacterSize(SIZE_TEXT_COORD);
    id_text.setFillColor(COLOR_POINT);
    id_text.setStyle(COORD_TEXT_STYLE);

    point_id_texts.insert_or_assign(new_point.id, id_text);
}

void Workspace::add_line(any info) {
    cout << "add_line\n--------\n";
    Line_drawable new_line = any_cast< Line_drawable >(info);
    lines.insert_or_assign(new_line.id, new_line);

    sf::Text id_text;
    id_text.setString(std::to_string(new_line.id));
    id_text.setFont(*font);
    id_text.setCharacterSize(SIZE_TEXT_COORD);
    id_text.setFillColor(COLOR_LINE);
    id_text.setStyle(COORD_TEXT_STYLE);

    line_id_texts.insert_or_assign(new_line.id, id_text);
}

void Workspace::change_point(any info) {
    Point_drawable changed_p = any_cast< Point_drawable >(info);

    points[changed_p.id] = changed_p;
}
