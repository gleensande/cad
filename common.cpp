#include "settings.hpp"

template <typename T>
void vector_print(vector<T> v, string name) {
    cout << "-------------" << endl;
    cout << name << ": " << endl;
    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << " ";
    }
    cout << endl;
}

template void vector_print(vector<double>, string);
template void vector_print(vector<unsigned int>, string);
template void vector_print(vector<int>, string);
