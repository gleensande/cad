#include "ui.hpp"

void UI::create(sf::RenderWindow* iwindow) {
	window = iwindow;
    bus_ptr = Bus::get_instance();
    
    create_buttons();

    if (!font.loadFromFile("media/arial.ttf")) {
        cout << "Error with fonts" << endl;
        return;
    }
    create_icon();
    ws.create(window, &font);
    input.create(window, &font);
    errors.create(window, &font);
}

void UI::handle_events() {
    sf::Event event;

    while (window->pollEvent(event)) {
        if (event.type != sf::Event::Resized) {
            if (window->getSize().x < 720) {
                window->setSize({720, window->getSize().y});
            }
            if (window->getSize().y  < 480) {
                window->setSize({window->getSize().x, 480});
            }
        }

        switch(event.type) {
            case sf::Event::MouseMoved : on_mouse_moved(); break;
            case sf::Event::Closed : on_closed(); break;
            case sf::Event::Resized : on_resized(event); break;
            case sf::Event::MouseButtonReleased : on_click(event); break;
            case sf::Event::TextEntered : on_text_entered(event); break;
            case sf::Event::KeyReleased: on_key(event); break;
        }
    }
}

void UI::on_closed() {
    window->close();
}

void UI::on_mouse_moved() {
    ws.update_coordinates_text();
    for (int i = 0; i < BUTTON_COUNT; i++) {
        up_menu[i].on_mouse_moved();
    }
}

void UI::on_resized(sf::Event event) {
    sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
    window->setView(sf::View(visibleArea));

    ws.update();

    float block_x = 1.f / 64.f * window->getSize().x;
    float block_y = 1.f / 32.f * window->getSize().y;

    float side = block_y < block_x ? block_y * 4.5 : block_x * 4.5;
    sf::Vector2<float> btn_size = {side, side};
    float step = 0, space = (window->getSize().x - BUTTON_COUNT * side) / 2;
    for (int i = 0; i < BUTTON_COUNT; i++) {
        up_menu[i].set_position({step + space, block_y});
        up_menu[i].set_size(btn_size);
        space += step + btn_size.x;
    }

    input.on_resize();    
    errors.on_resize();
}

void UI::on_text_entered(sf::Event event) {
    char c = static_cast<char>(event.text.unicode);
    input.on_text_entered(c);
}

void UI::on_key(sf::Event event) {
    if (event.key.code == 58) {
        input.on_enter();
    }
}


void UI::draw() {
    window->clear(COLOR_WINDOW); 
    for(int i = 0; i < BUTTON_COUNT; i++) {
        up_menu[i].draw();
        
    }
    ws.draw();
    input.draw();
    errors.draw();
    window->display();
}

void UI::create_buttons() {
    ifstream buttons_settings;
    buttons_settings.open(buttons_settings_path);
    if (!buttons_settings) {
        cout << "ОШИБКА: Невозможно открыть файл c данными : " << buttons_settings_path << endl;
        return;
    }

    float block_x = 1.f / 64.f * window->getSize().x;
    float block_y = 1.f / 32.f * window->getSize().y;

    float side = block_y < block_x ? block_y * 4.5 : block_x * 4.5;
    sf::Vector2<float> btn_size = {side, side};
    float step = 0, space = (window->getSize().x - BUTTON_COUNT * side) / 2;
    int i = 0;
    string line;
    while(getline(buttons_settings, line)) {
        vector<string> info = CSV_Parser::parse_line(line, "\t");
        up_menu[i].create(info, {step + space, block_y}, window, btn_size, &font);
        space += step + btn_size.x;
        i++;
    }
}

void UI::on_click(sf::Event event) {
    Message msg = {"click", make_any< sf::Vector2<float> >(event.mouseButton.x, event.mouseButton.y)};
    bus_ptr->send_message(msg);
}

void UI::create_icon(){
    if(!icon.loadFromFile("media/icons/main_icon/main1.png")){  
		cout << "Error: can't load the app icon from file" << endl;
		return;
	}
    window->setIcon(128, 128, icon.getPixelsPtr());
}
