#include "figures.hpp"

Point::Point(int _x, int _y, unsigned int _id) {
    bus_ptr = Bus::get_instance();

    x = _x;
    y = _y;
    id = _id;

    // создание объекта отрисовки
    Point_drawable a;
    a.x = x;
    a.y = y;
    a.id = id;

    // отправка объекта отрисовки в workspace
    Message msg = {"add_point_to_ws", make_any< Point_drawable >(a)};
    bus_ptr->send_message(msg);

    cout << "Created point: { id = " << id << ", x = " << x << ", y = " << y << " }\n"; 
}

void Point::add_x(int inc_x) {
    x += inc_x;

    Point_drawable a;
    a.x = x;
    a.y = y;
    a.id = id;

    // отправка обновления отрисовки в workspace
    Message msg = {"change_point_in_ws", make_any< Point_drawable >(a)};
    bus_ptr->send_message(msg);

    cout << "Changed point: { id = " << id << ", x = " << x << ", y = " << y << " }\n";
}

void Point::add_y(int inc_y) {
    y += inc_y;

    Point_drawable a;
    a.x = x;
    a.y = y;
    a.id = id;

    // отправка обновления отрисовки в workspace
    Message msg = {"change_point_in_ws", make_any< Point_drawable >(a)};
    bus_ptr->send_message(msg);

    cout << "Changed point: { id = " << id << ", x = " << x << ", y = " << y << " }\n";
}

void Point::change_coordinates(int new_x, int new_y) {
    x = new_x;
    y = new_y;

    // создание нового объекта отрисовки с новыми координатами и старым id
    Point_drawable a;
    a.x = x;
    a.y = y;
    a.id = id;

    // отправка обновления отрисовки в workspace
    Message msg = {"change_point_in_ws", make_any< Point_drawable >(a)};
    bus_ptr->send_message(msg);

    cout << "Changed point: { id = " << id << ", x = " << x << ", y = " << y << " }\n";
}

int Point::get_x() const {
    return x;
}

int Point::get_y() const {
    return y;
}

unsigned int Point::get_id() const {
    return id;
}


Line::Line(Point* _point1, Point* _point2, unsigned int _id) {
    bus_ptr = Bus::get_instance();
    
    point1 = _point1;
    point2 = _point2; 
    id = _id;

    // создание объекта отрисовки
    Line_drawable a;
    a.p1_id = point1->get_id();
    a.p2_id = point2->get_id();
    a.id = _id;

    cout << "Point 1 = " <<point1->get_x() << "; " << point1->get_y() << ";\n";

    // отправка объекта отрисовки в workspace  
    Message msg = {"add_line_to_ws", make_any< Line_drawable >(a)};
    bus_ptr->send_message(msg);

    cout << "line created" << endl;
}

void Line::change_point1(int x, int y) {
    point1->change_coordinates(x, y);
}

void Line::change_point2(int x, int y) {
    point2->change_coordinates(x, y);
}

unsigned int Line::get_id() const {
    return id;
}

int Line::get_x1()  {
    return  point1->get_x();
}
int Line::get_x2()  {
    return  point2->get_x();
}
int Line::get_y1()  {
    return  point1->get_y();
}
int Line::get_y2()  {
    return  point2->get_y();
}

vector<unsigned int> Line::get_points_ids() {
    vector<unsigned int> ids;
    ids.push_back(point1->get_id());
    ids.push_back(point2->get_id());

    return ids;
}