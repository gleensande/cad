#include "csv_parser.hpp"

// Разделяет строку на массив строк по переданному разделителю,
// сокращая пробелы на концах разделенных строк
vector<string> CSV_Parser::parse_line(string line, string delimiter) {
    vector<string> splited_line;

    line = trim_spaces(line);
    line += "\n";

    size_t prev_pos = 0, delimiter_pos = line.find(delimiter);
    string sub_line;
    while (delimiter_pos != string::npos) {
        sub_line = line.substr(prev_pos, delimiter_pos - prev_pos);
        splited_line.push_back(trim_spaces(sub_line));

        prev_pos = delimiter_pos + delimiter.size();
        delimiter_pos = line.find(delimiter, prev_pos);
    }
    splited_line.push_back(trim_spaces(line.substr(prev_pos)));

    return splited_line;
}

// Убирает пробелы с концов переданной строки
string CSV_Parser::trim_spaces(string line) {
    string trimmed;
    string space_like_characters = " \t\n";

    size_t first_non_space = line.find_first_not_of(space_like_characters);
    size_t last_non_space = line.find_last_not_of(space_like_characters);

    trimmed = line.substr(first_non_space, last_non_space - first_non_space + 1);

    return trimmed;
}


// Парсинг файла в формате ключ(строка)-значение(вещественное число)
map<string, double> CSV_Parser::parse_kv_file(string filename) {
    map<string, double> result;

    // Открытие файла
    ifstream data_file;
    data_file.open(filename);
    if (!data_file) {
        cout << "ОШИБКА: Невозможно открыть файл c данными : " << filename << endl;
        return result;
    }


    // заполнение словаря
    string line;
    while(getline(data_file, line)) {
        vector<string> name_and_value = parse_line(line, "=");
        string name = name_and_value[0];
        double value = stod(name_and_value[1]);

        result[name] = value;
    }

    return result;
}

vector<int> CSV_Parser::parse_num_line(string line, string delimiter) {
    vector<string> res_str = parse_line(line, delimiter);
    int size = res_str.size();
    vector<int> res_int(size);

    for (int i = 0; i < size; i++) {
        res_int[i] = stoi(res_str[i]);
    }

    return res_int;
}
