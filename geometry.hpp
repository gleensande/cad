#pragma once
#include "settings.hpp"
#include "bus.hpp"
#include "figures.hpp"
#include "constraints.hpp"
#include "csv_parser.hpp"
#include "parameter.hpp"
#include "newton_solver.hpp"

class Geometry {
 private:
    Bus* bus_ptr;
    map<unsigned int, Point> points;
    map<unsigned int, Line> lines;
    unsigned int object_id;
    
    vector<Constraint*> constraints;

    unsigned int param_id;
    vector<Parameter*> params;
    vector<Parameter*> get_params_by_point_id(unsigned int _id);
    
    bool wait_for_input;
    int state;

    // решение системы для ограничений
    Newton_solver NS;
    vector<unsigned int> Link_param_X; // хранятся id параметров в том же порядке, что в X
    vector< vector<unsigned int> > Link_constr_param; // ключ - номер ограничения в списке ограничений, 
                                                      // значение - вектор id используемых им параметров 

    int n; // суммарное количество неизвестных параметров d_p
    int m; // суммарная размерность всех уравнений ограничений = количество коэффициентов Лагранжа lambda

 public:
    void create();
    int  create_point(int x, int y);
    void create_line(int x1, int y1, int x2, int y2);
    void create_points_distance(unsigned int id_1, unsigned int id_2, float distance);
    
    void create_point_to_point(unsigned int id_1, unsigned int id_2);
    void create_parallel(unsigned int id_1, unsigned int id_2);
    void create_perpend(unsigned int id_1, unsigned int id_2);
    void create_anglelimit(unsigned int id_1, unsigned int id_2, float angle);
    void create_gorizont(unsigned int id_1);
    void create_vertical(unsigned int id_1);
    void create_point_to_line(unsigned int id_1, unsigned int id_2);

    void on_add_linesegment_pressed(any info);
    void on_add_point_pressed(any info);
    void on_input_finished(any info);

    void on_add_pointtopoint_pressed(any info);
    void on_add_distancelimit_pressed(any info);
    void on_add_parallellimit_pressed(any info);
    void on_add_perpendlimit_pressed(any info);
    void on_add_anglelimit_pressed(any info);
    void on_add_gorizontlimit_pressed(any info);
    void on_add_verticallimit_pressed(any info);
    void on_add_pointtolinelimit_pressed(any info);

    void solve_constraints();
    vector<double> F(vector<double>& X);
    vector<double> check_F(vector<double>& X);
    vector<int> find_x_indices_by_param_ids(vector<unsigned int>& param_ids) const;
    void print_params();
    void print_constraints();

    ~Geometry();
};
