#include "error.hpp"

void Errors::create(sf::RenderWindow* iwindow, sf::Font* ifont) {
    font = ifont;
    window = iwindow;
    
    display = false;
    
    description_error_text.setFont(*font);
    description_error_text.setCharacterSize(SIZE_DESCRIPTION_TEXT);
    description_error_text.setFillColor(COLOR_INPUT_TEXT);

    
    main_rect.setFillColor(COLOR_INPUT); 
    main_rect.setOutlineColor(COLOR_INPUT_OUTLINE_ER);
    main_rect.setOutlineThickness(THICKNESS_OUTLINE_ER);
    
    // close btn
    sf::Vector2<float> btn_position = {0, 0};
    vector<string> info = {"close", "close_error", "cancel.png", "Close"};
    sf::Vector2<float> btn_size = {20, 20};

    close_btn.create(info, btn_position, window, btn_size, font);
    bus_ptr = Bus::get_instance();
    bus_ptr->subscribe("error_sms", bind(&Errors::on_error_sms, this, std::placeholders::_1));
    bus_ptr->subscribe("close_error", bind(&Errors::on_close_error, this, std::placeholders::_1));
}

void Errors::draw() {
    if (display) {
        window->draw(main_rect);
        
        window->draw(description_error_text);
        close_btn.draw();
    }
}

void Errors::set_description(string idescription_error) {
    description_error = idescription_error;
    description_error_text.setString(description_error);

    float height = description_error_text.getLocalBounds().height;
    float width = description_error_text.getLocalBounds().width;
    sf::Vector2<float> text_size = {width, height};
    sf::Vector2u window_size = window->getSize();

    size = {text_size.x + 2*ERROR_SPACE,  text_size.y + 2*ERROR_SPACE};

    position = {window_size.x / 2 - size.x / 2, window_size.y / 2 - size.y / 2};
    description_error_text.setPosition({position.x  + ERROR_SPACE, position.y + 3*ERROR_SPACE/4});
    
    main_rect.setSize(size);
    main_rect.setPosition(position);

    sf::Vector2<float> btn_position = {position.x + size.x - 22, position.y + 2};
    close_btn.set_position(btn_position);
}

void Errors::on_resize() {
    set_description(description_error);
}

void Errors::switch_display() {
    display = !display;
}

void Errors::on_close_error(any info){
    display = false;
}

void Errors::on_error_sms(any info){
    string description = any_cast<string>(info);
    set_description(description);
    switch_display();
}
